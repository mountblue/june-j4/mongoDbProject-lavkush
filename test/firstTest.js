const expect = require('chai').expect
const path = require('path')
const fileName = path.resolve("../src/first.js")
const dataBase = 'matches';
const collectionDataBase = 'testMatchData';
const methodeCall = require(fileName)
describe('question One Test', function () {
    it('data file should have data', async function () {
        const expectResult = [];
        let result = await methodeCall.getTotalMatchPerYear(dataBase, collectionDataBase);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('server should be connected', async function () {
        const expectResult = true;
        let result = await methodeCall.getTotalMatchPerYear(dataBase, collectionDataBase);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('fetched data should be match', async function () {
        const expectResult = [{
                label: 2015,
                y: 2
            },
            {
                label: 2016,
                y: 2
            },
            {
                label: 2017,
                y: 2
            }
        ];
        let result = await methodeCall.getTotalMatchPerYear(dataBase, collectionDataBase);
        expect(result).to.deep.equal(expectResult);
    })
})