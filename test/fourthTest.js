const expect = require('chai').expect
const path = require('path')
const fileName = path.resolve("../src/fourth.js")
const dataBase = 'matches';
const collectionDataBase = 'testMatchData';
const methodeCall = require(fileName)
describe('question One Test', function () {
    it('data file should have data', async function () {
        const expectResult = [];
        let result = await methodeCall.getEconomyRatePerBowler(dataBase, collectionDataBase);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('server should be connected', async function () {
        const expectResult = true;
        let result = await methodeCall.getEconomyRatePerBowler(dataBase, collectionDataBase);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('fetched data should be match', async function () {
        const expectResult = [{
                label: 'A Mishra',
                y: 5
            },
            {
                label: 'AD Russell',
                y: 6
            },
            {
                label: 'AD Mathews',
                y: 7.5
            },
            {
                label: 'Yuvraj Singh',
                y: 7.5
            },
            {
                label: 'UT Yadav',
                y: 7.5
            },
            {
                label: 'SP Narine',
                y: 7.75
            },
            {
                label: 'PP Chawla',
                y: 8
            },
            {
                label: 'STR Binny',
                y: 8.25
            },
            {
                label: 'JA Morkel',
                y: 8.333333333333334
            },
            {
                label: 'Z Khan',
                y: 8.5
            }
        ];
        let result = await methodeCall.getEconomyRatePerBowler(dataBase, collectionDataBase);
        expect(result).to.deep.equal(expectResult);
    })
})