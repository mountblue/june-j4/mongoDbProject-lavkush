const expect = require('chai').expect
const path = require('path')
const fileName = path.resolve("../src/second.js")
const dataBase = 'matches';
const collectionDataBase = 'testMatchData';
const methodeCall = require(fileName)
describe('question One Test', function () {
    it('data file should have data', async function () {
        const expectResult = [];
        let result = await methodeCall.getWinMatcheesPerTeamInPerYear(dataBase, collectionDataBase);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('server should be connected', async function () {
        const expectResult = true;
        let result = await methodeCall.getWinMatcheesPerTeamInPerYear(dataBase, collectionDataBase);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('fetched data should be match', async function () {
        const expectResult = [{
                teams: 'Rising Pune Supergiant',
                season: 2017,
                count: 1
            },
            {
                teams: 'Sunrisers Hyderabad',
                season: 2017,
                count: 1
            },
            {
                teams: 'Kolkata Knight Riders',
                season: 2015,
                count: 1
            },
            {
                teams: 'Sunrisers Hyderabad',
                season: 2016,
                count: 2
            },
            {
                teams: '',
                season: 2015,
                count: 1
            }
        ];
        let result = await methodeCall.getWinMatcheesPerTeamInPerYear(dataBase, collectionDataBase);
        expect(result).to.deep.equal(expectResult);
    })
})