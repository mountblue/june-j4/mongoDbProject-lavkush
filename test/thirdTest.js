const expect = require('chai').expect
const path = require('path')
const fileName = path.resolve("../src/third.js")
const dataBase = 'matches';
const collectionDataBase = 'testMatchData';
const methodeCall = require(fileName)
describe('question One Test', function () {
    it('data file should have data', async function () {
        const expectResult = [];
        let result = await methodeCall.getTotalExtraRunPerTeam(dataBase, collectionDataBase);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('server should be connected', async function () {
        const expectResult = true;
        let result = await methodeCall.getTotalExtraRunPerTeam(dataBase, collectionDataBase);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('fetched data should be match', async function () {
        const expectResult = [{
                label: 'Gujarat Lions',
                y: 2
            },
            {
                label: 'Royal Challengers Bangalore',
                y: 14
            },
            {
                label: 'Sunrisers Hyderabad',
                y: 14
            }
        ];
        let result = await methodeCall.getTotalExtraRunPerTeam(dataBase, collectionDataBase);
        expect(result).to.deep.equal(expectResult);
    })
})