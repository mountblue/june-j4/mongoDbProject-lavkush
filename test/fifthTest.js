const expect = require('chai').expect
const path = require('path')
const fileName = path.resolve("../src/fifth.js")
const dataBase = 'matches';
const collectionDataBase = "deliveriesTest";
const methodeCall = require(fileName)
describe('question One Test', function () {
    it('data file should have data', async function () {
        const expectResult = [];
        let result = await methodeCall.getTopTenWicket(dataBase, collectionDataBase);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('server should be connected', async function () {
        const expectResult = true;
        let result = await methodeCall.getTopTenWicket(dataBase, collectionDataBase);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('fetched data should be match', async function () {
        const expectResult = [{
                label: 'STR Binny',
                y: 0
            },
            {
                label: 'MJ McClenaghan',
                y: 0
            },
            {
                label: 'TS Mills',
                y: 0
            },
            {
                label: 'KA Pollard',
                y: 0
            },
            {
                label: 'JJ Bumrah',
                y: 0
            },
            {
                label: 'UT Yadav',
                y: 0
            },
            {
                label: 'DS Kulkarni',
                y: 0
            },
            {
                label: 'B Kumar',
                y: 0
            },
            {
                label: 'S Kaushik',
                y: 0
            },
            {
                label: 'DJ Bravo',
                y: 0
            }
        ];
        let result = await methodeCall.getTopTenWicket(dataBase, collectionDataBase);
        expect(result).to.deep.equal(expectResult);
    })
})