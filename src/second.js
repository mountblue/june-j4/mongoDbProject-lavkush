// 2nd Question//////////////////////////////////////////////////////////////////////////
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://127.0.0.1:27017';

function getWinMatcheesPerTeamInPerYear(dbName, collectionData) {
    let fetchQuery = [{
            $group: {
                "_id": {
                    "winner": "$winner",
                    "season": "$season"
                },
                "count": {
                    $sum: 1
                }
            }
        },
        {
            $project: {
                _id: 0,
                season: "$_id.season",
                teams: "$_id.winner",
                count: "$count"
            }
        }
    ]
    return new Promise(function (resolve, recject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, client) {
            if (err) {
                resolve(true);
            } else {
                let dataBase = client.db(dbName);
                let collectionName = dataBase.collection(collectionData);
                collectionName.aggregate(fetchQuery).toArray(function (err, docs) {
                    if (err) {
                        console.log("somthing wrongs!")
                    } else {
                        client.close();
                        ///////////////////////****for creating json File//////////////////////////////
                        // require("fs").writeFile("../jsonFile/second.json", JSON.stringify(docs, null, 9), (err) => {
                        //     if (err) {
                        //         console.log("json not craeted");
                        //         return;
                        //     }
                        // });
                        resolve(docs);
                    }
                })
            }
        })
    })
}
getWinMatcheesPerTeamInPerYear('matches', 'matchdata');
module.exports = {
    getWinMatcheesPerTeamInPerYear: getWinMatcheesPerTeamInPerYear,
}