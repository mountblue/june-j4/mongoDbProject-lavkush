// 4th Question//////////////////////////////////////////////////////////////////////////
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://127.0.0.1:27017';

function getEconomyRatePerBowler(dbName, collectionData) {
    let fetchQuery = [{
            $lookup: {
                from: "deliveriesdata",
                localField: "id",
                foreignField: "match_id",
                as: "joinData"
            }
        },
        {
            $match: {
                "season": 2015
            }
        },
        {
            $unwind: "$joinData"
        },
        {
            $group: {
                "_id": "$joinData.bowler",
                "total_runs": {
                    $sum: "$joinData.total_runs"
                },
                "all_balls": {
                    $sum: {
                        $cond: {
                            if: {
                                $gt: ["$joinData.wide_runs", 0]
                            },
                            then: 0,
                            else: {
                                $cond: {
                                    if: {
                                        $gt: ["$joinData.noball_runs", 0]
                                    },
                                    then: 0,
                                    else: 1
                                }
                            }
                        }
                    }
                },
            }
        },
        {
            $project: {
                "economyRate": {
                    $divide: [{
                        $multiply: ["$total_runs", 6]
                    }, "$all_balls"]
                }
            }
        },
        {
            $sort: {
                "economyRate": 1
            },
        },
        {
            $limit: 10
        },
        {
            $project: {
                _id: "$null",
                "label": "$_id",
                "y": "$economyRate"
            }
        }
    ]
    return new Promise(function (resolve, recject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, client) {
            if (err) {
                resolve(true);
            } else {
                let dataBase = client.db(dbName);
                let collectionName = dataBase.collection(collectionData);
                collectionName.aggregate(fetchQuery).toArray(function (err, docs) {
                    if (err) {
                        console.log("somthing wrongs!")
                    } else {
                        client.close();
                        resolve(docs);
                    }
                })
            }
        })
    })
}
getEconomyRatePerBowler('matches', 'matchdata');

module.exports = {
    getEconomyRatePerBowler: getEconomyRatePerBowler,
}