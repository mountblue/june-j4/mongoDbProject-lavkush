// 3rd Question//////////////////////////////////////////////////////////////////////////
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://127.0.0.1:27017';

function getTotalExtraRunPerTeam(dbName, collectionData) {
    let fetchQuery = [{
            $match: {
                "season": 2016
            }
        },
        {
            $lookup: {
                from: "deliveriesdata",
                localField: "id",
                foreignField: "match_id",
                as: "joinData"
            }
        },
        {
            $unwind: "$joinData"
        },
        {
            $project: {
                "_id": 0,
                "joinData.bowling_team": 1,
                "joinData.extra_runs": 1
            }
        },
        {
            $group: {
                "_id": "$joinData.bowling_team",
                "runs": {
                    $sum: "$joinData.extra_runs"
                }
            }
        },
        {
            $project: {
                _id: "$null",
                "label": "$_id",
                "y": "$runs"
            }
        }
    ]
    return new Promise(function (resolve, recject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, client) {
            if (err) {
                resolve(true);
            } else {
                let dataBase = client.db(dbName);
                let collectionName = dataBase.collection(collectionData);
                collectionName.aggregate(fetchQuery).toArray(function (err, docs) {
                    if (err) {
                        console.log("somthing wrongs!")
                    } else {
                        client.close();
                        //    require("fs").writeFile("../jsonFile/extraRun.json", JSON.stringify(docs, null, 9), (err) => {
                        //         if (err) {
                        //             console.log("json not craeted");
                        //             return;
                        //         }
                        //     });
                        resolve(docs);
                    }
                })
            }
        })
    })
}
getTotalExtraRunPerTeam('matches', 'matchdata');

module.exports = {
    getTotalExtraRunPerTeam: getTotalExtraRunPerTeam,
}