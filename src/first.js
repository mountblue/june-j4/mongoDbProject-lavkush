const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://127.0.0.1:27017';

// 1st Question//////////////////////////////////////////////////////////////////////////
function getTotalMatchPerYear(dbName, collectionData) {
    let fetchQuery = [{
            $group: {
                "_id": "$season",
                totalCount: {
                    $sum: 1
                }
            }
        },
        {
            $sort: {
                "_id": 1
            }
        },
        {
            $project: {
                _id: "$null",
                "label": "$_id",
                "y": "$totalCount"
            }
        }
    ]
    return new Promise(function (resolve, recject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, client) {
            if (err) {
                resolve(true);
            } else {
                let dataBase = client.db(dbName);
                let collectionName = dataBase.collection(collectionData);
                collectionName.aggregate(fetchQuery).toArray(function (err, docs) {
                    if (err) {
                        console.log("somthing wrongs!")
                    } else {
                        client.close();
                        // require("fs").writeFile("../jsonFile/second.json", JSON.stringify(docs, null, 9), (err) => {
                        //     if (err) {
                        //         console.log("json not craeted");
                        //         return;
                        //     }
                        // });
                        resolve(docs);
                    }
                })
            }
        })
    })
}
getTotalMatchPerYear('matches', 'matchdata');

module.exports = {
    getTotalMatchPerYear: getTotalMatchPerYear,
}