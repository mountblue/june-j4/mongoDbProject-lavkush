const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://127.0.0.1:27017';

function getTopTenWicket(dbName, collectionData) {
    let fetchQuery = [{
        $group: {
            '_id': "$bowler",
            "wicket": {
                $sum: {
                    $cond: {
                        if: {
                            $ne: ["$player_dismissed", ""]
                        },
                        then: 1,
                        else: 0
                    }
                }
            }
        }
    }, {
        $sort: {
            "wicket": -1
        }
    }, {
        $limit: 10

    },
    {
        $project: {
            _id: "$null",
            "label": "$_id",
            "y": "$wicket"
        }
    }
]
    return new Promise(function (resolve, recject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, client) {
            if (err) {
                resolve(true);
            } else {
                let dataBase = client.db(dbName);
                let collectionName = dataBase.collection(collectionData);
                collectionName.aggregate(fetchQuery).toArray(function (err, docs) {
                    if (err) {
                        console.log("somthing wrongs!")
                    } else {
                        client.close();
                        require("fs").writeFile("../jsonFile/topWicketTaker.json", JSON.stringify(docs, null, 9), (err) => {
                            if (err) {
                                console.log("json not craeted");
                                return;
                            }
                        });
                        resolve(docs);
                    }
                })
            }
        })
    })
}
getTopTenWicket('matches', 'deliveriesdata');

module.exports = {
    getTopTenWicket: getTopTenWicket,
}