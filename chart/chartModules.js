function loadDoc(fileName) {
    let data = [];
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            data = JSON.parse(this.responseText);
        }
    };
    xhttp.open("GET", fileName, false);
    xhttp.send();
    return data;
}

function matchPerSeason() {
    let matchPerSeason = loadDoc("../jsonFile/matchesPerSeason.json");
    var chart = new CanvasJS.Chart("chartContainer0", {
        animationEnabled: true,
        theme: "light1", // "light1", "light2", "dark1", "dark2"
        title: {
            text: "Match Per season"
        },
        axisY: {
            title: "Number of match"
        },
        data: [{
            type: "column",
            legendMarkerColor: "grey",
            dataPoints: matchPerSeason,
        }]
    });
    chart.render();
}
matchPerSeason();

function teamsWinsmatchesPerYear() {
    let teamsWin = loadDoc("../jsonFile/second.json");
    console.log(teamsWin);

    let yearArray = [];
    let teamNameArray = []
    let teamWonObj = {};


    teamsWin.forEach(function (data) {
        let teamName = data["teams"];
        let year = data["season"];
        let wins = data["count"];
        if (teamName.length > 0) {
            if (!teamNameArray.includes(teamName))
                teamNameArray.push(teamName);
            if (teamWonObj.hasOwnProperty(teamName)) {
                teamWonObj[teamName][year] = wins;
            } else {
                teamWonObj[teamName] = {};
                for (let yr = 2008; yr < 2018; yr++) {
                    teamWonObj[teamName][yr] = 0;
                }
                teamWonObj[teamName][year] = wins;
            }
        }
    })
    let json = [];
    teamNameArray.forEach(function (teamName) {
        json.push({
            name: teamName,
            data: Object.values(teamWonObj[teamName])
        })
    })
    console.log(json);
    let years = loadDoc("../jsonFile/teamPerYear.json");
    Highcharts.chart('containerbar', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Stacked bar chart for Match Wins Per Team'
        },
        xAxis: {
            categories: years
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Match Wins'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: json,
    });
}
teamsWinsmatchesPerYear()

function extraPerTeam() {
    let extraPerTeam = loadDoc("../jsonFile/extraRun.json");

    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        theme: "light1", // "light1", "light2", "dark1", "dark2"
        title: {
            text: "Extra Runs Per Team In 2016"
        },
        axisY: {
            title: "Runs Per Team(2016)"
        },
        data: [{
            type: "column",
            legendMarkerColor: "grey",
            dataPoints: extraPerTeam,
        }]
    });
    chart.render();
}
extraPerTeam();

function bowlerEconmyRate() {
    let bowlerEconomyRate = loadDoc("../jsonFile/economyRate.json");
    var chart = new CanvasJS.Chart("chartContainer1", {
        theme: "light1", // "light1", "light2", "dark1", "dark2"
        title: {
            text: "Economy Rate Of Bowlers"
        },
        axisY: {
            title: "Economy Rate"
        },
        data: [{
            type: "column",
            legendMarkerColor: "grey",
            yValueFormatString: "#,##0.#",
            dataPoints: bowlerEconomyRate,
        }]
    });
    chart.render();
}
bowlerEconmyRate();


function topTenWicketTaker() {
    let topTen = loadDoc("../jsonFile/topWicketTaker.json");
    var chart = new CanvasJS.Chart("chartContainer2", {
        theme: "light1", // "light1", "light2", "dark1", "dark2"
        title: {
            text: "Top wicket Taker in all Years"
        },
        axisY: {
            title: "Wickets"
        },
        data: [{
            type: "column",
            legendMarkerColor: "grey",
            dataPoints: topTen,
        }]
    });
    chart.render();
}
topTenWicketTaker();